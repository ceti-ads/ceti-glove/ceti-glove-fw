#include <micro_ros_arduino.h>

#include <rcl/rcl.h>
#include <rcl/error_handling.h>
#include <rclc/rclc.h>
#include <rclc/executor.h>
#include <rosidl_runtime_c/string_functions.h>

//Includes for the ROS system 
#include "rosidl_runtime_c/primitives_sequence.h"
#include "rosidl_runtime_c/primitives_sequence_functions.h"
#include "rcl/service.h"

#include <TensorFlowLite_ESP32.h>
#include "tensorflow/lite/micro/all_ops_resolver.h"
#include "tensorflow/lite/micro/micro_error_reporter.h"
#include "tensorflow/lite/micro/micro_interpreter.h"
#include "tensorflow/lite/micro/system_setup.h"
#include "tensorflow/lite/schema/schema_generated.h"
#include "tensorflow/lite/c/common.h"

// Messages
#include <sensor_msgs/msg/joint_state.h>
#include <std_msgs/msg/int16.h>
#include <std_msgs/msg/float32_multi_array.h>

#include "../../include/smartGloveMain.hpp"

///////////////////////////MicroRos def ///////////////////
extern "C" int clock_gettime(clockid_t unused, struct timespec *tp);

rclc_executor_t executor;
rclc_support_t support;
rcl_allocator_t allocator;
rcl_node_t node;
rcl_timer_t timer;
rcl_service_t min_max_service;

////Service///
cetiglove_msgs__srv__MinMax_Request req ; 
cetiglove_msgs__srv__MinMax_Response res;

////Regression///
rcl_publisher_t reg_publisher;
sensor_msgs__msg__JointState joint_state_msg;


////Classification///
rcl_publisher_t class_publisher;
std_msgs__msg__Int16 msg;

// SmartGlove
// rcl_publisher_t float_pub;
rcl_publisher_t glove_pub;
rcl_publisher_t gloveStatus_pub;

////Verification///
rcl_publisher_t verif_publisher;
std_msgs__msg__Int16 Verif_msg;

SmartGlove smartGloveInstance;

cetiglove_msgs__msg__SmartGloveDataMSG smartGloveData;
cetiglove_msgs__msg__SmartGloveStatusMSG smartGloveStatus;

// std_msgs__msg__Float32MultiArray RawData_msg; 

// Adafruit_BNO055 bno = Adafruit_BNO055(55, 0x28);

/////////////////TFlite def ///////////////
////Regression///
constexpr int reg_kTensorArenaSize = TENSOR_ARENA_SIZE_REGRESSION;
tflite::ErrorReporter* reg_error_reporter = nullptr;
const tflite::Model* reg_model = nullptr;
tflite::MicroInterpreter* reg_interpreter = nullptr;
TfLiteTensor* reg_input = nullptr;
TfLiteTensor* reg_output = nullptr;
int reg_inference_count = 0;
uint8_t reg_tensor_arena[reg_kTensorArenaSize];

////Classification///
constexpr int class_kTensorArenaSize = TENSOR_ARENA_SIZE_CLASSIFICATION;
tflite::ErrorReporter* class_error_reporter = nullptr;
const tflite::Model* class_model = nullptr;
tflite::MicroInterpreter* class_interpreter = nullptr;
TfLiteTensor* class_input = nullptr;
TfLiteTensor* class_output = nullptr;
int class_inference_count = 0;
uint8_t class_tensor_arena[class_kTensorArenaSize];


/////////////////Variable declaration ///////////////
////Regression///
float reg_X_test[5] ={0};
int8_t X_test_quant[5] = {0};
int8_t y_pred_quant[10] = {0};
size_t num_joints;
uint32_t Index = 0;

////Classification///
const float accelerationThreshold = 20.5; // threshold of significant in G's
const int numSamples = 65;
int samplesRead = 65;
constexpr int MAX_MEASUREMENTS = 65; // Number of samples to keep in each axis
float measurements[23][MAX_MEASUREMENTS];
float mad[23];
float class_X_test[23] ;
int i = 0 ;

int firstTime = 0 ;
int Calibration = 0 ;
float y_pred[10] = {0};

void error_loop(){
  while(1){
    delay(100);
  }
}

void min_max_callback(const void* request, void* response) {
  cetiglove_msgs__srv__MinMax_Request* req = (cetiglove_msgs__srv__MinMax_Request*)request;
  cetiglove_msgs__srv__MinMax_Response* res = (cetiglove_msgs__srv__MinMax_Response*)response;
  // smartGloveInstance.flexsensors_update( &smartGloveData ) ;

  if( req->value == 0 )
  {
    smartGloveInstance.setFlexMax();
    // smartGloveInstance.flexsensors_getRAWFingers(max_values);

    Calibration = 1 ;
  }
  else if ( req->value == 1 )
  {
    smartGloveInstance.setFlexMin();
    // smartGloveInstance.flexsensors_getRAWFingers(min_values);

    Calibration = 2 ;
    
  }
  else
  {
    res->success = false;  
    res->message.data = "Wrong Request";
  }
  
  res->success = true;
  if ( req->value == 0 )
    res->message.data = "Max values updated successfully";
  else
    res->message.data = "Min values updated successfully";
}

void MicroRos_setup () {
  allocator = rcl_get_default_allocator();

  //create init_options
  RCCHECK(rclc_support_init(&support, 0, NULL, &allocator));

  // create node
  RCCHECK(rclc_node_init_default(&node, "imu_publisher_node", "", &support));

  // create Regression publisher
  RCCHECK(rclc_publisher_init_best_effort(
  &reg_publisher,
  &node,
  ROSIDL_GET_MSG_TYPE_SUPPORT(sensor_msgs, msg, JointState),
  "CetiGlove/ml/joints")); 

  // create Classification publisher
  RCCHECK(rclc_publisher_init_best_effort(
  &class_publisher,
  &node,
  ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Int16),
  "CetiGlove/ml/classifcation"));

  // Service MinMax
  RCCHECK(rclc_service_init_best_effort(
  &min_max_service,
  &node,
  ROSIDL_GET_SRV_TYPE_SUPPORT(cetiglove_msgs, srv, MinMax),
  "CetiGlove/min_max_srv"));

  // RCCHECK(rclc_publisher_init_best_effort(
  // &float_pub,
  // &node,
  // ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Float32MultiArray),
  // "RawData_pub"));
  
  RCCHECK(rclc_publisher_init_best_effort(
  &glove_pub,
  &node,
  ROSIDL_GET_MSG_TYPE_SUPPORT(cetiglove_msgs, msg, SmartGloveDataMSG),
  "CetiGlove/data"));

  RCCHECK(rclc_publisher_init_best_effort(
  &gloveStatus_pub,
  &node,
  ROSIDL_GET_MSG_TYPE_SUPPORT(cetiglove_msgs, msg, SmartGloveStatusMSG),
  "CetiGlove/status"));
}



#ifdef Classification

void Classification_Model_setup () {
    static tflite::MicroErrorReporter class_micro_error_reporter;
    class_error_reporter = &class_micro_error_reporter;

    class_model = tflite::GetModel(classification_model);

    static tflite::AllOpsResolver class_resolver;

    // Build an interpreter to run the model with.
    static tflite::MicroInterpreter class_static_interpreter(
    class_model, class_resolver, class_tensor_arena, class_kTensorArenaSize, class_error_reporter);
    class_interpreter = &class_static_interpreter;

    TfLiteStatus class_allocate_status = class_interpreter->AllocateTensors();

    class_input = class_interpreter->input(0);
    class_output = class_interpreter->output(0);
    class_inference_count = 0;

}

void Inference(){
     while (samplesRead == numSamples) {
       imu::Vector<3> accelerometer = bno.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);
      
    // sum up the absolutes
       float aSum = fabs(accelerometer.x()) + fabs(accelerometer.y()) + fabs(accelerometer.z());
             
    // check if it's above the threshold
       if (aSum >= accelerationThreshold) {
    // reset the sample read count
       samplesRead = 0;
       break;
         }
      }
       
      while (samplesRead < numSamples) {
      smartGloveInstance.IMU_update( &smartGloveData ) ;
      smartGloveInstance.flexsensors_update( &smartGloveData ) ;
       
      measurements[0][samplesRead] = smartGloveData.imu.accelerometer.x ;
      measurements[1][samplesRead] = smartGloveData.imu.accelerometer.y ;
      measurements[2][samplesRead] = smartGloveData.imu.accelerometer.z ;
      measurements[3][samplesRead] = smartGloveData.imu.gyroscope.x ;
      measurements[4][samplesRead] = smartGloveData.imu.gyroscope.y ;
      measurements[5][samplesRead] = smartGloveData.imu.gyroscope.z ;
      measurements[6][samplesRead] = smartGloveData.imu.magnetometer.x ;
      measurements[7][samplesRead] = smartGloveData.imu.magnetometer.y ;
      measurements[8][samplesRead] = smartGloveData.imu.magnetometer.z ;

      measurements[9][samplesRead] = smartGloveData.imu.absolute_orientation.x ;
      measurements[10][samplesRead] = smartGloveData.imu.absolute_orientation.y ;
      measurements[11][samplesRead] = smartGloveData.imu.absolute_orientation.z ;

      measurements[12][samplesRead] = smartGloveData.imu.linearaccel.x ;
      measurements[13][samplesRead] = smartGloveData.imu.linearaccel.y ;
      measurements[14][samplesRead] = smartGloveData.imu.linearaccel.z ;

      measurements[15][samplesRead] = smartGloveData.imu.gravity.x ;
      measurements[16][samplesRead] = smartGloveData.imu.gravity.y ;
      measurements[17][samplesRead] = smartGloveData.imu.gravity.z ;

      measurements[18][samplesRead] = smartGloveInstance.applyLowPassFilter(smartGloveData.flexsensor.thumb_raw,0) ;
      measurements[19][samplesRead] = smartGloveInstance.applyLowPassFilter(smartGloveData.flexsensor.index_raw,1);

      measurements[20][samplesRead] =  smartGloveInstance.applyLowPassFilter(smartGloveData.flexsensor.middle_raw,2);
      measurements[21][samplesRead] =  smartGloveInstance.applyLowPassFilter(smartGloveData.flexsensor.annular_raw,3);
      measurements[22][samplesRead] =  smartGloveInstance.applyLowPassFilter(smartGloveData.flexsensor.pinky_raw,4);
 
      samplesRead++;
      
      }
     
      for (int axis = 0; axis < 23; axis++) 
      {
       normalize(measurements[axis], 65);
       class_X_test[axis] = 1 * calc_mad(measurements[axis], MAX_MEASUREMENTS);     
      } 

      if (class_input->bytes != sizeof(float) * kModelInputSize) {
      //  Serial.println("Input tensor size mismatch");
      }
        
      // Copy the normalized data into the input tensor
      memcpy(class_input->data.f, class_X_test, sizeof(float) * kModelInputSize);

      // Run inference, and report any error
      TfLiteStatus invoke_status = class_interpreter->Invoke();
      if (invoke_status != kTfLiteOk) {
        class_error_reporter->Report("Invoke failed");
        return;
      }

      // Ensure the output tensor size matches your model's output size.
      if (class_output->bytes != sizeof(float) * kModelOutputSize) {
       //   Serial.println("Output tensor size mismatch");  
      }

      int max_index = 0;
      float max_value = class_output->data.f[0];
      for (int i = 1; i < kModelOutputSize; i++) {
          if (class_output->data.f[i] > max_value) {
              max_value = class_output->data.f[i];
              max_index = i;
          }
      }
      
      switch(max_index) 
      {
       case 0:
         msg.data = 0 ;
        // Serial.println("left") ;
         break;
       case 1:
         msg.data = 1 ;
        // Serial.println("striaght") ;
         break;
       case 2:
         msg.data = 2 ;
       //  Serial.println("stop") ;
         break;
       case 3:
         msg.data = 3 ;
       //  Serial.println("Right") ;
         break;
       case 4:
         msg.data = 4 ;
        // Serial.println("Turn") ;
         break;
       default:
         break;
      }
    RCSOFTCHECK(rcl_publish(&class_publisher, &msg, NULL));

  
}
#elif defined Regression


void Regression_Model_setup () {
  static tflite::MicroErrorReporter reg_micro_error_reporter;
  reg_error_reporter = &reg_micro_error_reporter;

  reg_model = tflite::GetModel(regression_model);

  static tflite::AllOpsResolver reg_resolver;

  // Build an interpreter to run the model with.
  static tflite::MicroInterpreter static_interpreter(
  reg_model, reg_resolver, reg_tensor_arena, reg_kTensorArenaSize, reg_error_reporter);
  reg_interpreter = &static_interpreter;

  TfLiteStatus allocate_status = reg_interpreter->AllocateTensors();

  reg_input = reg_interpreter->input(0);
  reg_output = reg_interpreter->output(0);
  reg_inference_count = 0;
}

void Inference(){
   
  // smartGloveInstance.min_max_normalization(reg_X_test, min_values, max_values, 5);
  reg_X_test[0] = smartGloveData.flexsensor.thumb_norm;
  reg_X_test[1] = smartGloveData.flexsensor.index_norm;
  reg_X_test[2] = smartGloveData.flexsensor.middle_norm;
  reg_X_test[3] = smartGloveData.flexsensor.annular_norm;
  reg_X_test[4] = smartGloveData.flexsensor.pinky_norm;


  for(int i = 0; i < 5; i++) {
    X_test_quant[i] = reg_X_test[i] / reg_input->params.scale + reg_input->params.zero_point ;   
  }
  
  // Copy the normalized data into the input tensor
  memcpy(reg_input->data.int8, X_test_quant, sizeof(X_test_quant));

  // Run inference, and report any error
  TfLiteStatus invoke_status = reg_interpreter->Invoke();
  if (invoke_status != kTfLiteOk) {
    reg_error_reporter->Report("Invoke failed");
    return;
  }

  // Copy the output tensor to y_pred
  memcpy(y_pred_quant, reg_output->data.int8, sizeof(y_pred_quant));

  for (int i = 0; i < 10; i++) {
      y_pred[i] = (float(y_pred_quant[i]) - float(reg_output->params.zero_point)) * reg_output->params.scale;
  }

  // Initialize the position sequence
  rosidl_runtime_c__float64__Sequence__init(&joint_state_msg.position, num_joints);

  // Assign the y_pred values to the joint_state_msg.position.data
  for (size_t i = 0; i < 10; i++) {
      joint_state_msg.position.data[i] = smartGloveInstance.applyLowPassFilterOutput(y_pred[i],i) ;
  
  }
  
  rcl_publish(&reg_publisher, &joint_state_msg, NULL);
  free(joint_state_msg.position.data);

    
}
#else
#endif


void setup() {
  Serial.begin(115200) ;
  
  Serial.println("\n\n--- Start Glove Firmware ---");
  // Wire.begin();
  // delay(4000);
  
  //////// initialize the IMU  ////////
  smartGloveInstance.IMU_setup () ;

  smartGloveInstance.flexsensors_setup();

  set_microros_wifi_transports(
    /* Wifi SSID */
    "chello-desktop-PC-wifi", //"chello-ThinkPad-E14-Gen-3", // "iPhone 11 (2)", 
    /* Password */
    "robcartest", // "K0hpkcji", // "12346789", 
    /* Required IP */
    "10.42.0.1",// "172.20.10.2", 
    8888
    ); // here change you wifi name and password and your PC ID

  cetiglove_msgs__srv__MinMax_Request__init(&req);
  cetiglove_msgs__srv__MinMax_Response__init(&res);
  MicroRos_setup () ;
  
  #ifdef Classification
  Classification_Model_setup ();
  #elif defined Regression
  Regression_Model_setup ();
  #endif  

  bool success = sensor_msgs__msg__JointState__init(&joint_state_msg);

  const char *joint_names[] = {"Thumb_Base_MCP", "Thumb_MCP_IP", "Index_Base_MCP", "Index_MCP_PIP", "Middle_Base_MCP", "Middle_MCP_PIP", "Ring_Base_MCP", "Ring_MCP_PIP", "Pinky_Base_MCP", "Pinky_MCP_PIP"};
  num_joints = sizeof(joint_names) / sizeof(joint_names[0]);

  rosidl_runtime_c__String__Sequence__init(&joint_state_msg.name, num_joints) ;

  for (size_t i = 0; i < num_joints; i++) 
  {  
      rosidl_runtime_c__String__assign(&joint_state_msg.name.data[i], joint_names[i]) ;    
  }

/*
  std_msgs__msg__Float32MultiArray__init(&RawData_msg);

  RawData_msg.layout.dim.data = new std_msgs__msg__MultiArrayDimension;

  RawData_msg.layout.dim.size = 1;  
  RawData_msg.layout.dim.capacity = 1;  

  RawData_msg.layout.dim.data[0].size = 5;
  RawData_msg.layout.dim.data[0].stride = 5;
  RawData_msg.layout.dim.data[0].label.data = "RawData";
  RawData_msg.layout.dim.data[0].label.size = strlen(RawData_msg.layout.dim.data[0].label.data);
  RawData_msg.layout.dim.data[0].label.capacity = RawData_msg.layout.dim.data[0].label.size;
*/
  smartGloveStatus.flexsensors.capacity = 15;
  smartGloveStatus.imu.capacity = 15;
}

int seq_data = 0;
int seq_status = 0;
int prev_time = 0;

void loop() {
  u64_t time_millis = millis();
  u64_t time_s      = time_millis/1e3;
  u64_t time_nanos  = time_millis*1e6;

  
  smartGloveInstance.flexsensors_update( &smartGloveData ) ;
  smartGloveInstance.IMU_update( &smartGloveData ) ;

  smartGloveData.header.stamp.sec = time_s;
  smartGloveData.header.stamp.nanosec = time_nanos;
  smartGloveData.header.frame_id.data = "CeTIglove";
  
  rcl_publish(&glove_pub, &smartGloveData, NULL);

  // Inference() ;

  if (time_s > prev_time+1){
    smartGloveInstance.smartGloveStatus_update( &smartGloveStatus );
    rcl_publish(&gloveStatus_pub, &smartGloveStatus, NULL);  
    prev_time = time_s;
  }

  // rmw_request_id_t req_header;
  // if(rcl_take_request(&min_max_service, &req_header, &req) == RCL_RET_OK)
  // {
  //  min_max_callback(&req, &res);
  //  rcl_send_response(&min_max_service, &req_header, &res);
  // }

}
