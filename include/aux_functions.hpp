#ifndef AUX_FUNCTIONS
#define AUX_FUNCTIONS

void wait_print_secs(int seconds) {
  
  for(int i = 1; i <= seconds; i++) {

    Serial.print(i);
    Serial.print("...");

    delay(1000);
  }

  Serial.println(" ");
  Serial.println(" ");
}

#endif
