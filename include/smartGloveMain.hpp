#ifndef SMARTGLOVE
#define SMARTGLOVE

/*** Includes and definitions ***/

//...for the custom smartGlove ROS 2 MSG which holds the data from the sensors of the smartGlove
#include <cetiglove_msgs/msg/smart_glove_data_msg.h>
//...for the custom smartGlove ROS 1 MSG which holds the status from the sensors of the smartGlove
#include <cetiglove_msgs/msg/smart_glove_status_msg.h>
#include <cetiglove_msgs/srv/min_max.h>
#include "rosidl_runtime_c/string_functions.h"
#include "rosidl_runtime_c/primitives_sequence.h"
#include "rosidl_runtime_c/primitives_sequence_functions.h"
#include <stdio.h>


extern "C" {
#include "utils.h"
};
#include <driver/adc.h>
#include "aux_functions.hpp"

//Includes for the IMU Sensor (BNO055)
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>

//change to work with classification model or regression model
//#define Classification 
#define Regression 

#define SMARTGLOVE_DATA

#undef ESP32

#define SMARTGLOVE_FLEX_STORED_VALUES 100
#define SMARTGLOVE_FLEX_Norm_Min 0
#define SMARTGLOVE_FLEX_Norm_Max 100

#define DEBUG_SERIAL

#define N_fingers 5

#ifdef Classification
#define FILTER_ALPHA 0.15
#include "class_model.h"
#elif defined Regression

#include "reg_model.h"
#define FILTER_ALPHA 0.2
#endif

#define RCCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){error_loop();}}
#define RCSOFTCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){}}


constexpr int kModelInputSize = 23;
constexpr int kModelOutputSize = 5;

#define TENSOR_ARENA_SIZE_REGRESSION 25*1024 
#define TENSOR_ARENA_SIZE_CLASSIFICATION 22*1024

#define ARRAY_SIZE 10

// int min_values[5] = {855, 1600, 1419, 1053,   51};
// int max_values[5] = {3162, 4095, 4039, 3473, 1835};
// int8_t FirstCalibration = 0 ;

// Adafruit_BNO055 bno = Adafruit_BNO055(55);//, 0x28);

////////////////// MicroRos defintions //////////////
class SmartGlove
{
protected:

    //Instance of the Adafruit_BNO055 class to store state and access the functions of the IMU sensor 
    // Adafruit_BNO055 bno;
    Adafruit_BNO055 bno = Adafruit_BNO055(55, 0x28, &Wire);

    //Boolean which will hold the status of the IMU BNO005 sensor, which will be "true" if the sensor is connected.
    bool IMUStatus;

    //Integers which will hold the raw 12Bit data from the Flexsensors resistors for each finger
    uint16_t thumb;
    //int index is already declared in ~/esp32/Arduino.h:29, so better use indexFinger
    uint16_t indexFinger;
    uint16_t middle; 
    uint16_t annular;
    uint16_t pinky;

    //Integers which will hold the min. values from the Flexsensors resistors for each finger, which will get updated, everytime a new min. exists
    //Because the raw data from each Flexsensor is 12Bit long the max. value is 4095
    typedef struct {
      
      uint16_t RAWstate;
      uint16_t FILTstate;
      uint16_t MAPPEDstate;
      uint16_t MinPosition;
      uint16_t MaxPosition;
    } flex_struct_t;
    
    flex_struct_t flex_data[N_fingers]; // thumb, index, middle, ring, pinky
    
    //Boolean which will hold the status of the Flexsensors, which will be "true" if all Flexsensors channels are configured
    bool flexsensorsStatus;

    bool flexsensorsFirstTime = 0;

    const int min_values[5] = {855, 1600, 1419, 1053,   51};
    const int max_values[5] = {3162, 4095, 4039, 3473, 1835};

public:

    SmartGlove();
    ~SmartGlove();

    boolean IMU_setup();
    boolean flexsensors_setup();

    void IMU_update(cetiglove_msgs__msg__SmartGloveDataMSG* smartGloveDataMSG);
    void flexsensors_update(cetiglove_msgs__msg__SmartGloveDataMSG* smartGloveDataMSG);
    
    void flexsensors_getMappedFingers(int *mappedFingers);
    void flexsensors_getRAWFingers(int *RAWFingers);
    void smartGloveStatus_update(cetiglove_msgs__msg__SmartGloveStatusMSG* smartGloveStatusMSG);

    Adafruit_BNO055 getIMU();
    void setIMUStatus(bool sensorStatus);
    void setFlexMin();
    void setFlexMax();

    uint16_t applyLowPassFilter(uint16_t rawValue, int Index);
    uint16_t applyLowPassFilterOutput(float Value , int Index) ;
    void min_max_normalization(float X[], float min_values[], float max_values[], int n);
    uint16_t previousFilteredValue[5] = {0};
    float previousFilteredValueOutputs[10] = {0};
    
private:
 
    void flexsensors_calibration();
    bool flexsensors_read();

};

SmartGlove::SmartGlove() {

  thumb = 0;
  indexFinger = 0;
  middle = 0; 
  annular = 0;
  pinky = 0;

  for (int i = 0; i < N_fingers; i++) {
    flex_data[i].RAWstate = 0;
    flex_data[i].FILTstate = 0;
    flex_data[i].MAPPEDstate = 0;
    flex_data[i].MinPosition = min_values[i];
    flex_data[i].MaxPosition = max_values[i];    
  }

  IMUStatus = false; 
  flexsensorsStatus = false;
}

SmartGlove::~SmartGlove() {
}

/*** Setup functions ***/

//Set up the IMU Sensor (BNO055) 
boolean SmartGlove::IMU_setup () {

  // bno = Adafruit_BNO055(55, 0x28);
  //Try to begin the working of the sensor
  if(!bno.begin()) {

    Serial.println("No BNO055 detected!\n");

    delay(1000);

    return false;
  } else {

    Serial.println("BNO055 detected!\n");
    
    IMUStatus = true;

    delay(1000);

    return true;
  }

  bno.setExtCrystalUse(true);
}

//Set up the analog to digital conversion channels for every Flexsensor of each finger
boolean SmartGlove::flexsensors_setup() {

  delay(3000);

  //Holds the number of succesfully configured Flexsensors 
  int connectedFlexsensors = 0;

  //TODO: adc2_config_channel seems not to work when the WiFi module is used, look up for a solution!

  /* 
   *Configure the channels for the flex resistor sensors and set the attenuation 
   *ADC_ATTEN_DB_11 is for the suggested range of 0 - 2450 mV 
  */    

  //Initialization

  //Thumb
  if(adc1_config_channel_atten(ADC1_CHANNEL_3, ADC_ATTEN_DB_11) == ESP_OK) 
    connectedFlexsensors++;
  //Index
  if(adc1_config_channel_atten(ADC1_CHANNEL_0, ADC_ATTEN_DB_11) == ESP_OK) 
    connectedFlexsensors++;
  //Middlefinger
  if(adc1_config_channel_atten(ADC1_CHANNEL_6, ADC_ATTEN_DB_11) == ESP_OK)
    connectedFlexsensors++;
  //Ringfinger
  if(adc1_config_channel_atten(ADC1_CHANNEL_7, ADC_ATTEN_DB_11) == ESP_OK)
    connectedFlexsensors++;
  //Pinky
  if(adc1_config_channel_atten(ADC1_CHANNEL_4, ADC_ATTEN_DB_11) == ESP_OK)
    connectedFlexsensors++;
    
 // Serial.println(" ");

  if(connectedFlexsensors == 5) {
    
#ifdef DEBUG_SERIAL
  //  Serial.println("All channels for flex resistor were configured"); 
#endif
 //   flexsensors_calibration();
    flexsensorsStatus = true;

    return true;
  } else {

#ifdef DEBUG_SERIAL
  //  Serial.println("ERROR: Not all channels for flex resistor were configured"); 

    return false;
#endif
  }
}

/*** Update functions ***/

void SmartGlove::IMU_update(cetiglove_msgs__msg__SmartGloveDataMSG* smartGloveDataMSG) {

    sensors_event_t event;

    //Get all the possible data from the BNO055 sensor as vectors
    imu::Vector<3> absoluteOrientation = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
    imu::Vector<3> angularVelocity = bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE);
    imu::Vector<3> linearAcceleration = bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL);
    imu::Vector<3> gravity = bno.getVector(Adafruit_BNO055::VECTOR_GRAVITY);
    imu::Vector<3> accelerometer = bno.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);
    imu::Vector<3> magnetometer = bno.getVector(Adafruit_BNO055::VECTOR_MAGNETOMETER);

    //Pack the data from the BNO055 into the custom smartGlove ROS 1 IMU MSG
    smartGloveDataMSG->imu.absolute_orientation.x = absoluteOrientation.x();
    smartGloveDataMSG->imu.absolute_orientation.y = absoluteOrientation.y();
    smartGloveDataMSG->imu.absolute_orientation.z = absoluteOrientation.z();

    smartGloveDataMSG->imu.gyroscope.x = angularVelocity.x();
    smartGloveDataMSG->imu.gyroscope.y = angularVelocity.y();
    smartGloveDataMSG->imu.gyroscope.z = angularVelocity.z();

    smartGloveDataMSG->imu.linearaccel.x = linearAcceleration.x();
    smartGloveDataMSG->imu.linearaccel.y = linearAcceleration.y();
    smartGloveDataMSG->imu.linearaccel.z = linearAcceleration.z();

    smartGloveDataMSG->imu.gravity.x = gravity.x();
    smartGloveDataMSG->imu.gravity.y = gravity.y();
    smartGloveDataMSG->imu.gravity.z = gravity.z();

    smartGloveDataMSG->imu.accelerometer.x = accelerometer.x();
    smartGloveDataMSG->imu.accelerometer.y = accelerometer.y();
    smartGloveDataMSG->imu.accelerometer.z = accelerometer.z();

    smartGloveDataMSG->imu.magnetometer.x = magnetometer.x();
    smartGloveDataMSG->imu.magnetometer.y = magnetometer.y();
    smartGloveDataMSG->imu.magnetometer.z = magnetometer.z();
}

void SmartGlove::flexsensors_update(cetiglove_msgs__msg__SmartGloveDataMSG* smartGloveDataMSG) {

  //Pack the 12Bit raw data of the flexsensors resistors into the custom smartGlove ROS 1 flexsensors MSG
  //The 12Bit raw data is also getting normalized to the range between smartGlove_FLEX_Norm_Min and smartGlove_FLEX_Norm_Max
  if(flexsensors_read()) { 

    for (int i = 0; i < N_fingers; i++) {
      //Filtering
      flex_data[i].RAWstate = applyLowPassFilter(flex_data[i].RAWstate,i);

      //Normalization between smartGlove_FLEX_Norm_Min and smartGlove_FLEX_Norm_Max
      long mappedState = map(
        flex_data[i].RAWstate,
        flex_data[i].MinPosition,
        flex_data[i].MaxPosition,
        SMARTGLOVE_FLEX_Norm_Min,
        SMARTGLOVE_FLEX_Norm_Max);

      if(mappedState < SMARTGLOVE_FLEX_Norm_Min)
        mappedState = SMARTGLOVE_FLEX_Norm_Min;

      if(mappedState > SMARTGLOVE_FLEX_Norm_Max)
        mappedState = SMARTGLOVE_FLEX_Norm_Max;

      flex_data[i].MAPPEDstate = mappedState; 

      //Packing the raw and normalized values of each finger into the custom smartGlove ROS 1 MSG
      switch(i) {

        case 0: { //Thumb

          smartGloveDataMSG->flexsensor.thumb_raw = flex_data[i].RAWstate;//applyLowPassFilter(flex_data[i].RAWstate,0);
          smartGloveDataMSG->flexsensor.thumb_norm = flex_data[i].MAPPEDstate;
        } break;       

        case 1: { //Index
          
          smartGloveDataMSG->flexsensor.index_raw = flex_data[i].RAWstate;//applyLowPassFilter(flex_data[i].RAWstate,1);
          smartGloveDataMSG->flexsensor.index_norm = flex_data[i].MAPPEDstate;        
        } break;

        case 2: { //Middle
          smartGloveDataMSG->flexsensor.middle_raw = flex_data[i].RAWstate;//applyLowPassFilter(flex_data[i].RAWstate,2);
          smartGloveDataMSG->flexsensor.middle_norm = flex_data[i].MAPPEDstate;          
        } break;
      
        case 3: { //Annular
          
          smartGloveDataMSG->flexsensor.annular_raw = flex_data[i].RAWstate;//applyLowPassFilter(flex_data[i].RAWstate,3);
          smartGloveDataMSG->flexsensor.annular_norm = flex_data[i].MAPPEDstate;
        } break;

        case 4: { //Pinky

          smartGloveDataMSG->flexsensor.pinky_raw = flex_data[i].RAWstate;//applyLowPassFilter(flex_data[i].RAWstate,4);
          smartGloveDataMSG->flexsensor.pinky_norm = flex_data[i].MAPPEDstate;
        } break;

        default: 

          //
          break;
      }    
    }

#ifdef DEBUG_SERIAL

    if (Serial.available()) {

      Serial.flush();
    //  Serial.print("0,");

    }
#endif

  }
}

/*** Flexsensors functions ***/


//Reads the raw data from each Flexsensor resistor
bool SmartGlove::flexsensors_read() {

  byte updatedFlexsensors[N_fingers];

  for (int i = 0; i < N_fingers; i++)
    updatedFlexsensors[i] = 0;
    
  thumb = adc1_get_raw(ADC1_CHANNEL_3) ;
  updatedFlexsensors[0] = 1;

  indexFinger = adc1_get_raw(ADC1_CHANNEL_0) ;
  updatedFlexsensors[1] = 1;
  
  middle = adc1_get_raw(ADC1_CHANNEL_6) ;
  updatedFlexsensors[2] = 1;

  annular = adc1_get_raw(ADC1_CHANNEL_7) ;
  updatedFlexsensors[3] = 1;

  pinky = adc1_get_raw(ADC1_CHANNEL_4) ;
  updatedFlexsensors[4] = 1;
  
  flex_data[0].RAWstate = thumb;
  flex_data[1].RAWstate = indexFinger;
  flex_data[2].RAWstate = middle;
  flex_data[3].RAWstate = annular;
  flex_data[4].RAWstate = pinky;

  byte updatedFlexsensors_cont = 0;

  for (int i = 0; i < N_fingers; i++)
    updatedFlexsensors_cont += updatedFlexsensors[i];

  if(updatedFlexsensors_cont == N_fingers) { 
    return true;
  } else {
    flexsensorsStatus = false;

#ifdef DEBUG_SERIAL
  //Read the raw data from the resistors of the flexsensors and store them in an int
 // Serial.println("Some of the GPIOs in the flexsensor resistor are already in use!");
#endif

    return false;
  }

  if (!flexsensorsFirstTime){
    for (size_t i = 0; i < N_fingers; i++)
      previousFilteredValue[i] = flex_data[i].RAWstate;
  }
  
}

void SmartGlove::setFlexMin(){
  flexsensors_read();
  for (size_t i = 0; i < N_fingers; i++)
    flex_data[i].MinPosition = flex_data[i].RAWstate;
}

void SmartGlove::setFlexMax(){
  flexsensors_read();
  for (size_t i = 0; i < N_fingers; i++)
    flex_data[i].MaxPosition = flex_data[i].RAWstate;
}

void SmartGlove::flexsensors_getMappedFingers(int *mappedFingers) {

  for (int i = 0; i < N_fingers; i++) {

    mappedFingers[i] = flex_data[i].MAPPEDstate;
  }  
}

void SmartGlove::flexsensors_getRAWFingers(int *RAWFingers) {

  for (int i = 0; i < N_fingers; i++) {

    RAWFingers[i] = flex_data[i].RAWstate;
  }
}

/*** Status functions ***/

void SmartGlove::smartGloveStatus_update(cetiglove_msgs__msg__SmartGloveStatusMSG *smartGloveStatusMSG) {
  uint8_t tempIMUStatus;

  bno.getSystemStatus(0, &tempIMUStatus, 0);
  if(tempIMUStatus == 0x0F) {    
    IMUStatus = true;
    rosidl_runtime_c__String__assignn(&smartGloveStatusMSG->imu, "Connected",9);  
  }else{
    IMUStatus = false;
    rosidl_runtime_c__String__assignn(&smartGloveStatusMSG->imu, "Not Connected",13);
  }

  if(flexsensorsStatus == true) {
    rosidl_runtime_c__String__assignn(&smartGloveStatusMSG->flexsensors, "Connected",9);
  }else{
    rosidl_runtime_c__String__assignn(&smartGloveStatusMSG->flexsensors, "Not Connected",13);
  }
}

uint16_t SmartGlove::applyLowPassFilter(uint16_t rawValue , int Index) {
  uint16_t filteredValue = (uint16_t)((1 - FILTER_ALPHA) * previousFilteredValue[Index] + FILTER_ALPHA * rawValue);
  previousFilteredValue[Index] = filteredValue;
  return filteredValue;
}

uint16_t SmartGlove::applyLowPassFilterOutput(float Value , int Index) {
  uint16_t filteredValue = (uint16_t)((1 - FILTER_ALPHA) * previousFilteredValueOutputs[Index] + FILTER_ALPHA * Value);
  previousFilteredValueOutputs[Index] = filteredValue;
  return filteredValue;
}

void SmartGlove::min_max_normalization(float X[], float min_values[], float max_values[], int n) {
    for (int i = 0; i < n; i++) {
        X[i] = (X[i] - min_values[i]) / (max_values[i] - min_values[i]);
    }
}

#endif
